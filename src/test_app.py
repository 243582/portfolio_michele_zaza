import unittest
from tokenize import String
from app import app, db, Project
import json
from flask_login import login_user

def test_home_route():
    """!
    Funzione che testa il codice di risposta in seguito ad una GET alla
    homepage.
    """
    response = app.test_client().get('/')
    assert response.status_code == 200

def test_cv_route():
    """!
    Funzione che testa il codice di risposta in seguito ad una GET alla
    pagina del cv.
    """
    response = app.test_client().get('/cv')
    assert response.status_code == 200

def test_mz_route():
    """!
    Funzione che testa il codice di risposta in seguito ad una GET alla
    pagina Michele Zaza.
    """
    response = app.test_client().get('/mz')
    assert response.status_code == 200

def test_login_route():
    """!
    Funzione che testa il codice di risposta in seguito ad una GET alla
    pagina login admin.
    """
    response = app.test_client().get('/login')
    assert response.status_code == 200

app.config['LOGIN_DISABLED'] = True # Disabilito da qui in poi il login_required dell'admin per scopi di testing

def test_admin_zone_route():
    """!
    Funzione che testa il codice di risposta in seguito ad una GET alla
    pagina admin zone.
    """
    response = app.test_client().get('/admin_zone')
    assert response.status_code == 200

def test_insert_project():
    """!
    Funzione che testa il codice di risposta in seguito ad una POST alla
    pagina admin zone per l'aggiunta di progetti.
    """
    project = {'title':'Titolo di prova', 'content':'Contenuto di prova per testing'}
    response = app.test_client().post('/admin_zone', data=project)
    assert response.status_code == 302
    response = app.test_client().get('/admin_zone')
    assert 'Titolo di prova' in str(response.data)

def test_update_project():
    """!
    Funzione che testa il codice di risposta in seguito ad una POST alla
    pagina admin zone per l'aggiornamento di progetti del portfolio.
    """
    data = {'title':'Nuovo titolo di prova', 'content': 'Nuovo contenuto di prova aggiornato per il testing'}
    response = app.test_client().post('/update/1', data=data)
    assert response.status_code == 302

def test_delete_project():
    """!
    Funzione che testa il codice di risposta in seguito ad una GET alla
    pagina che elimina i progetti dal portfolio.
    """
    response = app.test_client().get("/delete/1")
    assert response.status_code == 302