#!/usr/bin/env python3

"""!
Portfolio di Michele Zaza realizzato con Flask, Flask-SQLAlchemy, Flask-Login
"""

from flask import Flask, render_template, request, redirect, send_from_directory, url_for
from flask_login import LoginManager, UserMixin # login
from flask_login import login_required, login_user, logout_user
from flask_sqlalchemy import SQLAlchemy

""" Variabile app Flask """
app = Flask(__name__)

# Chiave segreta
app.secret_key = 'key' # login

# Configurazione database
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # Disattivazione warning
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///pfdb.db'
db = SQLAlchemy(app)


# Login manager per entrare nell'area admin
login_manager = LoginManager(app)

# Inizializzazione login manager
login_manager.init_app(app)
login_manager.login_view = 'login'

# Utente admin abilitato all'accesso nell'area admin
users = {'admin':{'password':'admin'}}

# CLASSI ###############################################################################################################

class User(UserMixin):
    """!
    Classe User per creare l'utente admin che accede all'area riservata.
    UserMixin è una classe Flask che implementa, tra le altre cose, metodi per fare il login.
    Viene estesa la classe UserMixin.
    """
    pass

class Project(db.Model):
    """!
    Classe Project per la memorizzazione di progetti del portfolio.
    Campi: id per l'identificazione dei progetti, titolo del progetto e contenuto (descrizione del progetto).
    """
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    content = db.Column(db.String(300), nullable=False)

    def __rep__(self):
        return '<Project %r>' % self.id

########################################################################################################################

# FUNZIONI #############################################################################################################

@login_manager.user_loader
def user_loader(username):
    """!
    Metodo che controlla che l'utente che sta cercando di effettuare il login sia presente nella lista users
    (lista per ora comprendente solo l'admin).
    @param username = nome dell'utente da controllare.
    """
    if username not in users: # Se l'utente non è presente, non faccio nulla
        return

    user = User() # Se l'utente è presente inizializzo uno User, assegno come id il suo username e lo ritorno
    user.id = username
    return user

@login_manager.request_loader
def request_loader(request):
  """!
  Metodo che controlla che l'utente che sta cercando di accedere alla risorsa (area protetta) abbia i permessi per farlo
  @param request = richiesta
  """
  username = request.form.get('username')
  if username not in users:
    return

  user = User()
  user.id = username

  user.is_authenticated = request.form['password'] == users[username]['password'] # Qui viene accordato o meno il permesso

  return user

@app.route('/login', methods=['GET', 'POST'])
def login():
  """!
  Funzione che restituisce la pagina di login per entrare nell'area riservata (area admin).
  """
  if request.method == 'POST':
    username = request.form.get('username')
    if username in users and request.form.get('password') == users[username]['password']:
      user = User()
      user.id = username
      login_user(user)
      return redirect(url_for('admin_zone'))
    else:
        return render_template('admin_login_error.html')
  return render_template('login.html')

@app.route('/admin_zone', methods=['POST', 'GET'])
@login_required
def admin_zone():
    """!
    Funzione che restituisce la pagina dell'area riservata (area admin). Richiede il login per l'accesso.
    """
    if request.method == 'POST':
        project_title = request.form['title']
        project_content = request.form['content']
        new_project = Project(title=project_title, content=project_content)

        try:
            db.session.add(new_project)
            db.session.commit()
            return redirect("/admin_zone")
        except:
            return "There was an error while adding the project"
    else:
        projects = Project.query.all()
        return render_template('admin_zone.html', projects=projects)

@app.route('/logout')
def logout():
  """!
  Funzione che permette di effettuare il logout dall'area protetta (area admin).
  """
  logout_user()
  return redirect('/')

@app.route("/", methods=['POST','GET'])
def portfolio_page():
    """!
    Funzione che restituisce l'homepage del sito.
    """
    projects = Project.query.all()
    return render_template("home.html", projects=projects)

@app.route("/cv", methods=['GET'])
def cv_page():
    """!
    Funzione che restituisce la pagina cv.
    """
    return render_template("cv.html")

@app.route("/mz", methods=['GET'])
def mz_page():
    """!
    Funzione che restituisce la pagina Michele Zaza.
    """
    return render_template("mz.html")

@app.route('/delete/<int:id>')
@login_required
def delete(id):
    """!
    Funzione che cancella una entry del database.
    @param id = id dell'entry da eliminare.
    """
    project_to_delete = Project.query.get_or_404(id)
    try:
        db.session.delete(project_to_delete)
        db.session.commit()
        return redirect('/admin_zone')
    except:
        return 'There was an error while deleting that project'

@app.route('/update/<int:id>', methods=['GET','POST'])
@login_required
def update(id):
    """!
    Funzione che aggiorna una entry del database.
    @param id = id dell'entry da aggiornare.
    """
    project = Project.query.get_or_404(id)

    if request.method == 'POST':
        project.title = request.form['title']
        project.content = request.form['content']

        try:
            db.session.commit()
            return redirect('/admin_zone')
        except:
            return 'There was an issue while updating that project'

    else:
        return render_template('update.html', project=project)

@app.route('/documentation', methods=['GET'])
def documentation():
    """!
    Funzione che espone la documentazione.
    """
    return send_from_directory(path=".", directory=".", filename="doc.pdf", mimetype='application/pdf')

@app.route('/cvpdf', methods=['GET'])
def cvpdf():
    """!
    Funzione che espone il cv.
    """
    return send_from_directory(path=".", directory=".", filename="cv.pdf", mimetype='application/pdf')

########################################################################################################################

# MAIN #################################################################################################################

if __name__ == "__main__":
    app.run()

########################################################################################################################
