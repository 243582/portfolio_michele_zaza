Progetto "Portfolio Michele Zaza" per l'esame di "Cloud And Edge Computing" di Informatica (laurea magistrale) presso Unimore. Vengono sfruttate le seguenti tecnologie:
- Pipeline CI/CD di GitLab;
- Docker per la creazione di un container custom nella fase di pre-build;
- Flask, FlaskSQLAlchemy, Flask-Login per l'applicazione web; 
- Pytest per la fase di testing automatico;
- Doxygen per la fase di documentazione automatica;
- Heroku per il deploy;
- Telegram per le notifiche della pipeline.

Questa repository è stata creata ex-novo per la consegna del progetto.
